package ru.filit.train.repository;

import ru.filit.train.exceptios.CustomException;
import ru.filit.train.model.dim.BrandModel;
import org.apache.log4j.Logger;
import ru.filit.train.utils.PropertiesReader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Функционал работы со справочником брендов
 * */
public class BrandRepository implements Repository<BrandModel> {

    private static final ConnectionSupplier connectionSupplier = new ConnectionSupplier();
    private static final Logger logger = Logger.getLogger(BrandRepository.class.getName());
    private final String BRAND_TABLE_NAME;

    public BrandRepository(String propertyFileName){
        //считывание имени справочника из файла свойств
        BRAND_TABLE_NAME = PropertiesReader.read(propertyFileName).getProperty("dim_brand");
    }

    private BrandModel getModelFromSet(ResultSet set) throws SQLException {
        BrandModel model = new BrandModel();
        model.setId(set.getLong("id"));
        model.setName(set.getString("name"));
        return model;
    }

    public BrandModel findByName(String name){
        logger.info("Reading brand data from database...");
        BrandModel model = new BrandModel();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE NAME='%s'", BRAND_TABLE_NAME, name);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                model = getModelFromSet(resultSet);
            }
            logger.info("Brand data was read successfully");
        } catch (SQLException e) {
            throw new CustomException("Unable to read row from brand data", e);
        }
        return model;
    }

    @Override
    public BrandModel findById(long id) {
        logger.info("Reading brand data from database...");
        BrandModel model = new BrandModel();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE id=%d", BRAND_TABLE_NAME, id);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                model = getModelFromSet(resultSet);
            }
            logger.info("Brand data was read successfully");
        } catch (SQLException e) {
            throw new CustomException("Unable to read row from brand data", e);
        }
        return model;
    }

    @Override
    public BrandModel findLast(){
        logger.info("Reading brand data from database...");
        BrandModel model = new BrandModel();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE id=(SELECT MAX(id) FROM %s)", BRAND_TABLE_NAME, BRAND_TABLE_NAME);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                model = getModelFromSet(resultSet);
            }
            logger.info("Brand data was read successfully");
        } catch (SQLException e) {
            throw new CustomException("Unable to read row from brand data", e);
        }
        return model;
    }

    @Override
    public Collection<BrandModel> findAll() {
        logger.info("Reading brands data from database...");
        ArrayList<BrandModel> models = new ArrayList<>();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s", BRAND_TABLE_NAME);
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                models.add(getModelFromSet(resultSet));
            }
            logger.info("Brands data was read successfully");
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to read rows from brand data", e);
        }
        return models;
    }

    @Override
    public void removeById(long id) {
        logger.info("Removing row from brand data...");
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("DELETE FROM %s WHERE id=%d", BRAND_TABLE_NAME, id);
            statement.executeQuery(query);
            logger.info("Brand data row was deleted successfully");
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to delete row in brand data", e);
        }
    }

    @Override
    public BrandModel save(BrandModel model) {
        logger.info("Saving row to brand data");
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("INSERT INTO %s (NAME) VALUES('%s')",
                    BRAND_TABLE_NAME, model.getName());
            statement.executeQuery(query);
            logger.info("Brand data row was saved successfully");
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to save row in brand data", e);
        }
        return model;
    }

    @Override
    public void update(BrandModel model) {
        logger.info("Updating row to brand data");
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("UPDATE %s SET NAME='%s' WHERE ID=%d",
                    BRAND_TABLE_NAME, model.getName(), model.getId());
            statement.executeQuery(query);
            logger.info("Brand data row was updated successfully");
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to update row in brand data", e);
        }
    }
}
