package ru.filit.train.repository;

import ru.filit.train.exceptios.CustomException;
import ru.filit.train.utils.PropertiesReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Функционал подключения к Oracle
 */
public class ConnectionSupplier {

    //параметры подключения к oracle
    private final String URL;
    private final String USER;
    private final String PASSWORD;
    private static final String DEFAULT_PROPERTY_FILE = "db_connection.properties";

    public ConnectionSupplier() {
        Properties properties = PropertiesReader.read(DEFAULT_PROPERTY_FILE);
        URL = properties.getProperty("url");
        USER = properties.getProperty("user");
        PASSWORD = properties.getProperty("password");
    }

    public ConnectionSupplier(String propertyFileName){
        Properties properties = PropertiesReader.read(propertyFileName);
        URL = properties.getProperty("url");
        USER = properties.getProperty("user");
        PASSWORD = properties.getProperty("password");
    }

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            throw new CustomException("Unable to establish database connection", e);
        }
    }
}
