package ru.filit.train.repository;

import ru.filit.train.exceptios.CustomException;
import ru.filit.train.model.ProductModel;
import ru.filit.train.model.dim.BrandModel;
import org.apache.log4j.Logger;
import ru.filit.train.utils.PropertiesReader;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.Locale;

/**
 * Функционал работы с моделью продукта в oracle
 * */
public class ProductRepository implements Repository<ProductModel> {

    private final ConnectionSupplier connectionSupplier = new ConnectionSupplier();
    private static final Logger logger = Logger.getLogger(ProductRepository.class.getName());
    private final String productTableName;
    private final String product_group_TableName;
    private final String product_type_TableName;
    private final String product_brand_TableName;
    private final String dim_product_type_TableName;
    private final String dim_product_brand_TableName;
    private final String dim_product_group_TableName;

    public ProductRepository(String propertyFileName){
        //считывание имён таблиц из файла свойств
        Properties properties = PropertiesReader.read(propertyFileName);
        productTableName = properties.getProperty("product");
        product_group_TableName = properties.getProperty("product_group");
        product_type_TableName = properties.getProperty("product_type");
        product_brand_TableName = properties.getProperty("product_brand");
        dim_product_brand_TableName = properties.getProperty("dim_brand");
        dim_product_group_TableName = properties.getProperty("dim_product_group");
        dim_product_type_TableName = properties.getProperty("dim_product_type");
    }

    /**
     * Формирование класса продукта на основе данных из oracle
     * @param resultSet сет данных для парсинга
     * @param connection активное подключение к базе данных
     * @return сформированный класс аккаунта
     * */
    public ProductModel getModelFromResultSet(ResultSet resultSet, Connection connection) throws SQLException {
        ProductModel model = new ProductModel();
        model.setId(resultSet.getLong("id"));
        model.setArticle(resultSet.getString("article"));
        model.setDescription(resultSet.getString("description"));
        model.setPrice(resultSet.getDouble("price"));
        model.setGroup(getProductGroup(model.getId(), connection));
        model.setType(getProductType(model.getId(), connection));
        model.setBrand(getProductBrand(model.getId(), connection));
        return model;
    }

    private BrandModel getProductBrand(long productId, Connection connection) throws SQLException{
        BrandModel brand = new BrandModel();
        String query = String.format("SELECT * FROM %s WHERE PRODUCT_ID=%d", product_brand_TableName, productId);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        if(resultSet.next()){
            long brandId = resultSet.getLong("BRAND_ID");
            query = String.format("SELECT * FROM %s WHERE ID=%d", dim_product_brand_TableName, brandId);
            resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                brand.setId(resultSet.getLong("id"));
                brand.setName(resultSet.getString("name"));
            }
        }
        return brand;
    }

    private ProductModel.Group getProductGroup(long productId, Connection connection) throws SQLException {
        ProductModel.Group group = new ProductModel.Group();
        String query = String.format("SELECT * FROM %s WHERE PRODUCT_ID=%d", product_group_TableName, productId);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        if(resultSet.next()){
            group.id = resultSet.getLong("GROUP_ID");
            query = String.format("SELECT * FROM %s WHERE ID=%d", dim_product_group_TableName, group.id);
            resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                group.name = resultSet.getString("name");
            }
        }
        return group;
    }

    private ProductModel.Type getProductType(long productId, Connection connection) throws SQLException{
        ProductModel.Type type = new ProductModel.Type();
        String query = String.format("SELECT * FROM %s WHERE PRODUCT_ID=%d", product_type_TableName, productId);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        if(resultSet.next()){
            type.id = resultSet.getLong("TYPE_ID");
            query = String.format("SELECT * FROM %s WHERE ID=%d", dim_product_type_TableName, type.id);
            resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                type.name = resultSet.getString("name");
            }
        }
        return type;
    }

    @Override
    public ProductModel findById(long id) {
        logger.info("Reading product data from database...");
        ProductModel model = new ProductModel();
        try(Connection connection = connectionSupplier.getConnection()){
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE id=%d", productTableName, id);
            ResultSet resultSet = statement.executeQuery(query);
            logger.info("Product data was read successfully");
            if(resultSet.next()){
                model = getModelFromResultSet(resultSet, connection);
            }
            return model;
        } catch (CustomException | SQLException e){
            throw new CustomException("Unable to read rows from product data", e);
        }
    }

    @Override
    public ProductModel findLast(){
        logger.info("Reading product data from database...");
        ProductModel model = new ProductModel();
        try(Connection connection = connectionSupplier.getConnection()){
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE id=(SELECT MAX(id) FROM %s)", productTableName, productTableName);
            ResultSet resultSet = statement.executeQuery(query);
            logger.info("Product data was read successfully");
            if(resultSet.next()){
                model = getModelFromResultSet(resultSet, connection);
            }
            return model;
        } catch (CustomException | SQLException e){
            throw new CustomException("Unable to read rows from product data", e);
        }
    }

    @Override
    public Collection<ProductModel> findAll() {
        logger.info("Reading products data from database...");
        ArrayList<ProductModel> products = new ArrayList<>();
        try(Connection connection = connectionSupplier.getConnection()){
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s ", productTableName);
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                products.add(getModelFromResultSet(resultSet, connection));
            }
            return products;
        } catch (CustomException | SQLException e){
            throw new CustomException("Unable to read rows from product data", e);
        }
    }

    @Override
    public void removeById(long id) {
        logger.info("Removing row from product data...");
        try(Connection connection = connectionSupplier.getConnection()){
            Statement statement = connection.createStatement();
            String query;

            query = String.format("DELETE FROM %s WHERE PRODUCT_ID=%d", product_brand_TableName, id);
            statement.executeQuery(query);

            query = String.format("DELETE FROM %s WHERE PRODUCT_ID=%d", product_type_TableName, id);
            statement.executeQuery(query);

            query = String.format("DELETE FROM %s WHERE PRODUCT_ID=%d", product_group_TableName, id);
            statement.executeQuery(query);

            query = String.format("DELETE FROM %s WHERE ID=%d", productTableName, id);
            statement.executeQuery(query);

            logger.info("Product data row was deleted successfully");
        } catch (CustomException | SQLException e){
            throw new CustomException("Unable to remove product data", e);
        }
    }

    @Override
    public ProductModel save(ProductModel model) {
        logger.info("Saving row to product data...");
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format(Locale.US, "INSERT INTO %s (article, price, description) " +
                            "VALUES ('%s', %.2f, '%s')",
                    productTableName, model.getArticle(), model.getPrice(), model.getDescription());
            statement.executeQuery(query);

            long modelId = 0;
            query = String.format("SELECT MAX(id) as id FROM %s", productTableName);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                modelId = resultSet.getLong("id");
            }

            query = String.format("INSERT INTO %s (PRODUCT_ID, BRAND_ID) VALUES (%d, %d)", product_brand_TableName, modelId, model.getBrand().getId());
            statement.executeQuery(query);

            query = String.format("INSERT INTO %s (PRODUCT_ID, TYPE_ID) VALUES (%d, %d)", product_type_TableName, modelId, model.getType().id);
            statement.executeQuery(query);

            query = String.format("INSERT INTO %s (PRODUCT_ID, GROUP_ID) VALUES (%d, %d)", product_group_TableName, modelId, model.getGroup().id);
            statement.executeQuery(query);

            logger.info("Product data was saved successfully");
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to save row in product data", e);
        }
        return model;
    }

    @Override
    public void update(ProductModel model) {
        logger.info("Updating row in product data...");
        try(Connection connection = connectionSupplier.getConnection()){
            Statement statement = connection.createStatement();
            String query = String.format(Locale.US, "UPDATE %s SET ARTICLE='%s', DESCRIPTION='%s', PRICE=%.2f WHERE ID=%d",
                    productTableName, model.getArticle(), model.getDescription(), model.getPrice(), model.getId());
            statement.executeQuery(query);

            query = String.format("UPDATE %s SET BRAND_ID=%d, PRODUCT_ID=%d WHERE PRODUCT_ID=%d",
                    product_brand_TableName, model.getBrand().getId(), model.getId(), model.getId());
            statement.executeQuery(query);

            query = String.format("UPDATE %s SET TYPE_ID=%d, PRODUCT_ID=%d WHERE PRODUCT_ID=%d",
                    product_type_TableName, model.getType().id, model.getId(), model.getId());
            statement.executeQuery(query);

            query = String.format("UPDATE %s SET GROUP_ID=%d, PRODUCT_ID=%d WHERE PRODUCT_ID=%d",
                    product_group_TableName, model.getGroup().id, model.getId(), model.getId());
            statement.executeQuery(query);

            logger.info("Update row in product data was successfully");
        } catch (CustomException | SQLException e){
            throw new CustomException("Unable to update row in product data", e);
        }
    }
}
