package ru.filit.train.repository;

import java.util.Collection;

public interface Repository<T> {
    T findById(long id);
    Collection<T> findAll();
    void removeById(long id);
    T save(T model);
    void update(T model);
    T findLast();
}
