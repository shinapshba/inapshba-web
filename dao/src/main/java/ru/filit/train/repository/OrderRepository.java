package ru.filit.train.repository;

import ru.filit.train.exceptios.CustomException;
import ru.filit.train.model.OrderModel;
import ru.filit.train.model.ProductModel;
import org.apache.log4j.Logger;
import ru.filit.train.utils.PropertiesReader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class OrderRepository implements Repository<OrderModel> {

    private final ConnectionSupplier connectionSupplier = new ConnectionSupplier();
    private static final Logger logger = Logger.getLogger(OrderRepository.class.getName());
    private final String ORDERS_TABLE;
    private final String ORDER_PRODUCTS_TABLE;
    private final String PRODUCTS_TABLE;
    private final ProductRepository productRepository;

    public OrderRepository(String propertyFileName) {
        //считывание имён таблиц из файла свойств
        Properties properties = PropertiesReader.read(propertyFileName);
        ORDERS_TABLE = properties.getProperty("order");
        ORDER_PRODUCTS_TABLE = properties.getProperty("order_products");
        PRODUCTS_TABLE = properties.getProperty("product");

        productRepository = new ProductRepository(propertyFileName);
    }

    /**
     * Формирование класса заказа на основе данных из oracle
     * @param resultSet сет данных для парсинга
     * @return сформированный класс аккаунта
     * */
    private OrderModel getModelFromSet(ResultSet resultSet, Connection connection) throws SQLException {
        OrderModel model = new OrderModel();
        model.setId(resultSet.getLong("id"));
        model.setCustomerId(resultSet.getLong("customer_id"));
        model.setSellerId(resultSet.getLong("seller_id"));
        model.setCreationDate(LocalDate.parse(
                resultSet.getString("creation_date"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        model.setGeneralPrice(resultSet.getDouble("general_price"));
        model.setRestPrice(resultSet.getDouble("rest_price"));
        model.setDeliveryStatus(resultSet.getInt("delivery_status"));
        model.setPaid(resultSet.getInt("pay_status") == 1);
        model.setProducts(getOrderProducts(model.getId(), connection));
        return model;
    }

    private ArrayList<ProductModel> getOrderProducts(long orderId, Connection connection) throws SQLException {
        ArrayList<ProductModel> products = new ArrayList<>();
        ArrayList<Long> productIDes = new ArrayList<>();
        Statement statement = connection.createStatement();
        String query = String.format("SELECT PRODUCT_ID FROM %s WHERE ORDER_ID=%d", ORDER_PRODUCTS_TABLE, orderId);
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()){
            productIDes.add(resultSet.getLong("product_id"));
        }

        for (Long productId : productIDes) {
            query = String.format("SELECT * FROM %s WHERE id=%d", PRODUCTS_TABLE, productId);
            ResultSet resultSetProduct = statement.executeQuery(query);
            if(resultSetProduct.next()){
                products.add(productRepository.getModelFromResultSet(resultSetProduct, connection));
            }
        }
        return products;
    }

    @Override
    public OrderModel findById(long id) {
        logger.info("Reading order data from database...");
        OrderModel model = new OrderModel();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE id=%d", ORDERS_TABLE, id);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                model = getModelFromSet(resultSet, connection);
            }
        } catch (SQLException e) {
            throw new CustomException("Unable to read row from order data", e);
        }
        return model;
    }

    @Override
    public Collection<OrderModel> findAll() {
        logger.info("Reading orders data from database...");
        List<OrderModel> models = new ArrayList<>();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s", ORDERS_TABLE);
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                models.add(getModelFromSet(resultSet, connection));
            }
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to read rows from order data", e);
        }
        return models;
    }

    @Override
    public void removeById(long id) {
        logger.info("Removing row from order data...");
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();

            String query = String.format("DELETE FROM %s WHERE order_id=%d", ORDER_PRODUCTS_TABLE, id);
            statement.executeQuery(query);

            query = String.format("DELETE FROM %s WHERE id=%d", ORDERS_TABLE, id);
            statement.executeQuery(query);
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to delete data in order data", e);
        }
    }

    @Override
    public OrderModel save(OrderModel model) {
        logger.info("Saving row to order data...");
        try(Connection connection = connectionSupplier.getConnection()){
            Statement statement = connection.createStatement();
            int payStatus = model.isPaid() ? 1 : 0;
            String query = String.format(Locale.US, "INSERT INTO %s " +
                            "(customer_id, seller_id, delivery_status, general_price, rest_price, pay_status) " +
                            "VALUES (%d, %d, %d, %.2f, %.2f, %d)",
                    ORDERS_TABLE, model.getCustomerId(), model.getSellerId(), model.getDeliveryStatus(), model.getGeneralPrice(), model.getRestPrice(), payStatus);
            statement.executeQuery(query);

            long orderId = 0;
            query = String.format("SELECT MAX(id) as id FROM %s", ORDERS_TABLE);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                orderId = resultSet.getLong("id");
            }

            ArrayList<ProductModel> products = model.getProducts();
            if(products.size() != 0) {
                for (ProductModel product : products) {
                    query = String.format("INSERT INTO %s (order_id, product_id) VALUES(%d, %d)",
                            ORDER_PRODUCTS_TABLE, orderId, product.getId());
                    statement.executeQuery(query); } }
        } catch (CustomException | SQLException e){
            throw new CustomException("Unable to save data in order data", e);
        }
        return model;
    }

    @Override
    public void update(OrderModel model) {
        logger.info("Updating row in order data...");
        try(Connection connection = connectionSupplier.getConnection()){
            Statement statement = connection.createStatement();

            int pay_status = model.isPaid() ? 1 : 0;
            String query = String.format(Locale.US, "UPDATE %s SET customer_id=%d, seller_id=%d, delivery_status=%d, general_price=%.2f, rest_price=%.2f, pay_status=%d WHERE id=%d",
                    ORDERS_TABLE, model.getCustomerId(), model.getSellerId(), model.getDeliveryStatus(), model.getGeneralPrice(), model.getRestPrice(), pay_status, model.getId());
            statement.executeQuery(query);

            query = String.format("DELETE FROM %s WHERE ORDER_ID=%d", ORDER_PRODUCTS_TABLE, model.getId());
            statement.executeQuery(query);

            ArrayList<ProductModel> products = model.getProducts();
            if(products.size() != 0) {
                for (ProductModel product : model.getProducts()) {
                    query = String.format("INSERT INTO %s (ORDER_ID, PRODUCT_ID) VALUES(%d, %d)",
                            ORDER_PRODUCTS_TABLE, model.getId(), product.getId());
                    statement.executeQuery(query); } }
        } catch (SQLException e){
            throw new CustomException("Unable to update row in user data", e);
        }
    }

    @Override
    public OrderModel findLast(){
        logger.info("Reading order data from database...");
        OrderModel model = new OrderModel();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE id=(SELECT MAX(id) FROM %s)", ORDERS_TABLE, ORDERS_TABLE);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                model = getModelFromSet(resultSet, connection);
            }
        } catch (SQLException e) {
            throw new CustomException("Unable to read row from order data", e);
        }
        return model;
    }
}
