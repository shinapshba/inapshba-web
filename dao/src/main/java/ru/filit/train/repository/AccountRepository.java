package ru.filit.train.repository;

import ru.filit.train.exceptios.CustomException;
import ru.filit.train.model.AccountModel;
import org.apache.log4j.Logger;
import ru.filit.train.utils.PropertiesReader;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Функционал работы с моделью аккаунта пользователя в oracle
 * */
public class AccountRepository implements Repository<AccountModel> {

    private final ConnectionSupplier connectionSupplier = new ConnectionSupplier();
    private static final Logger logger = Logger.getLogger(AccountRepository.class.getName());
    private final String ACCOUNT_TABLE_NAME;

    public AccountRepository(String propertyFileName){
        //считывание имени таблицы аккаунтов файла свойств
        ACCOUNT_TABLE_NAME = PropertiesReader.read(propertyFileName).getProperty("account");
    }

    /**
     * Формирование класса аккаунта на основе данных из oracle
     * @param resultSet сет данных для парсинга
     * @return сформированный класс аккаунта
     * */
    public static AccountModel getModelFromSet(ResultSet resultSet) throws SQLException {
        AccountModel accountModel = new AccountModel();
        accountModel.setId(resultSet.getLong("id"));
        accountModel.setLogin(resultSet.getString("login"));
        accountModel.setEmail(resultSet.getString("email"));
        accountModel.setPhone(resultSet.getString("phone"));
        accountModel.setRegistrationDate(LocalDate.parse(resultSet.getString("registration_date"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        accountModel.setPassword_hash(resultSet.getString("password_hash"));
        accountModel.setEmailConfirmed(resultSet.getInt("email_confirmed") == 1);
        accountModel.setPhoneConfirmed(resultSet.getInt("phone_confirmed") == 1);
        accountModel.setActivity(resultSet.getInt("activity") == 1);
        accountModel.setRole(resultSet.getInt("role"));
        return accountModel;
    }

    public AccountModel findByLogin(String login){
        logger.info("Reading user account data from database...");
        AccountModel accountModel = new AccountModel();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE LOGIN='%s'", ACCOUNT_TABLE_NAME, login);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                accountModel = getModelFromSet(resultSet);
            }
        } catch (SQLException e) {
            throw new CustomException("Unable to read row from user data", e);
        }
        return accountModel;
    }

    @Override
    public AccountModel findById(long id) {
        logger.info("Reading user account data from database...");
        AccountModel accountModel = new AccountModel();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE id=%d", ACCOUNT_TABLE_NAME, id);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                accountModel = getModelFromSet(resultSet);
            }
        } catch (SQLException e) {
            throw new CustomException("Unable to read row from user data", e);
        }
        return accountModel;
    }

    @Override
    public AccountModel findLast(){
        logger.info("Reading user account data from database...");
        AccountModel accountModel = new AccountModel();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s WHERE id=(SELECT MAX(id) FROM %s)", ACCOUNT_TABLE_NAME, ACCOUNT_TABLE_NAME);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                accountModel = getModelFromSet(resultSet);
            }
        } catch (SQLException e) {
            throw new CustomException("Unable to read row from user data", e);
        }
        return accountModel;
    }

    @Override
    public Collection<AccountModel> findAll() {
        logger.info("Reading user accounts data from database...");
        List<AccountModel> accountModels = new ArrayList<>();
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("SELECT * FROM %s", ACCOUNT_TABLE_NAME);
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                accountModels.add(getModelFromSet(resultSet));
            }
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to read rows from user data", e);
        }
        return accountModels;
    }

    @Override
    public void removeById(long id) {
        logger.info("Removing row from user data...");
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            String query = String.format("DELETE FROM %s WHERE id=%d", ACCOUNT_TABLE_NAME, id);
            statement.executeQuery(query);
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to delete row in user data", e);
        }
    }

    @Override
    public AccountModel save(AccountModel model) {
        logger.info("Saving row to user data...");
        try (Connection connection = connectionSupplier.getConnection()) {
            Statement statement = connection.createStatement();
            int activity = model.isActive() ? 1 : 0;
            int emailConfirmed = model.isEmailConfirmed() ? 1 : 0;
            int phoneConfirmed = model.isPhoneConfirmed() ? 1 : 0;
            String query = String.format("INSERT INTO %s (login, email, phone, activity, email_confirmed, phone_confirmed, password_hash, role) " +
                    "VALUES ('%s', '%s', '%s', %d, %d, %d, '%s', %d)",
                    ACCOUNT_TABLE_NAME, model.getLogin(), model.getEmail(), model.getPhone(), activity, emailConfirmed, phoneConfirmed, model.getPassword_hash(), model.getRole());
            statement.executeQuery(query);
        } catch (CustomException | SQLException e) {
            throw new CustomException("Unable to save row in user data", e);
        }
        return model;
    }

    @Override
    public void update(AccountModel model) {
        logger.info("Updating row in user data...");
        try(Connection connection = connectionSupplier.getConnection()){
            Statement statement = connection.createStatement();
            int activity = model.isActive() ? 1 : 0;
            int emailConfirmed = model.isEmailConfirmed() ? 1 : 0;
            int phoneConfirmed = model.isPhoneConfirmed() ? 1 : 0;
            String query = String.format("UPDATE %s SET login='%s', email='%s', phone='%s', activity=%d, email_confirmed=%d, phone_confirmed=%d, password_hash='%s', role=%d WHERE id=%d",
                    ACCOUNT_TABLE_NAME, model.getLogin(), model.getEmail(), model.getPhone(), activity, emailConfirmed, phoneConfirmed, model.getPassword_hash(), model.getRole(), model.getId());
            statement.executeQuery(query);
        } catch (SQLException e){
            throw new CustomException("Unable to update row in user data", e);
        }
    }

    public AccountModel login(String userLogin, String passwordHash){
        logger.info(String.format("Login user '%s' ...", userLogin));
        try(Connection connection = connectionSupplier.getConnection()){
            Statement statement = connection.createStatement();
            String query = String.format("SELECT ID, PASSWORD_HASH FROM %s WHERE LOGIN='%s'",
                    ACCOUNT_TABLE_NAME, userLogin);
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                String passwordHash_db = resultSet.getString("password_hash");
                if(passwordHash.equals(passwordHash_db)){
                    return findByLogin(userLogin);
                } else {
                    throw new CustomException("Wrong password received", new Exception());
                }
            } else {
                throw new CustomException(String.format("No users with userLogin '%s'", userLogin), new Exception());
            }
        } catch (SQLException e){
            throw new CustomException(String.format("Unable to login user '%s'", userLogin), e);
        }
    }
}
