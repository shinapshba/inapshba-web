package ru.filit.train.model.dim;

import java.util.Objects;

/**
 * Модель справочника бренда продуктов
 * */
public class BrandModel {

    public BrandModel(){

    }

    public BrandModel(long id, String name){
        this.id = id;
        this.name = name;
    }

    public BrandModel(String name){
        this.name = name;
    }

    //region fields
    private long id;
    private String name;
    //endregion

    //region getters
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    //endregion

    //region setters
    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
    //endregion

    //region overrides
    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        BrandModel that = (BrandModel) o;
        return Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
    //endregion
}
