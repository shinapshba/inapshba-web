package ru.filit.train.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Модель данных аккаунта пользователя
 * */
public class AccountModel {

    //region fields
    private long id;
    private String login;
    private String password_hash;
    private String email;
    private String phone;
    private boolean emailConfirmed;
    private boolean phoneConfirmed;
    private LocalDate registrationDate;
    private boolean activity;
    private int role;
    //endregion

    //region getters
    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword_hash() {
        return password_hash;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public boolean isEmailConfirmed() {
        return emailConfirmed;
    }

    public boolean isPhoneConfirmed() {
        return phoneConfirmed;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public boolean isActive() {
        return activity;
    }

    public int getRole() {
        return role;
    }
    //endregion

    //region setters
    public void setId(long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword_hash(String password_hash) {
        this.password_hash = password_hash;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmailConfirmed(boolean emailConfirmed) {
        this.emailConfirmed = emailConfirmed;
    }

    public void setPhoneConfirmed(boolean phoneConfirmed) {
        this.phoneConfirmed = phoneConfirmed;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setActivity(boolean activity) {
        this.activity = activity;
    }

    public void setRole(int role) {
        this.role = role;
    }
    //endregion

    //region overrides
    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        AccountModel that = (AccountModel) o;
        return isEmailConfirmed() == that.isEmailConfirmed() &&
                isPhoneConfirmed() == that.isPhoneConfirmed() &&
                isActive() == that.isActive() &&
                getRole() == that.getRole() &&
                Objects.equals(getLogin(), that.getLogin()) &&
                Objects.equals(getPassword_hash(), that.getPassword_hash()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getPhone(), that.getPhone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLogin(), getPassword_hash(), getEmail(), getPhone(),
                isEmailConfirmed(), isPhoneConfirmed(), isActive(), getRole());
    }
    //endregion
}
