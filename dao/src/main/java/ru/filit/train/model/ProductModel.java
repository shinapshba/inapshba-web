package ru.filit.train.model;

import ru.filit.train.model.dim.BrandModel;

import java.util.Objects;

/**
 * Модель данных продукта
 * */
public class ProductModel {

    public ProductModel(){
        this.group = new Group();
        this.type = new Type();
        this.brand = new BrandModel();
    }

    //region entry
    public static class Group {
        public long id;
        public String name;
        public Group() { }
        public Group(long id, String name){
            this.id = id;
            this.name = name;
        }
    }

    public static class Type {
        public long id;
        public String name;
        public Type() { }
        public Type(long id, String name){
            this.id = id;
            this.name = name;
        }
    }
    //endregion

    //region fields
    private long id;
    private String article;
    private double price;
    private String description;
    private Group group;
    private Type type;
    private BrandModel brand;
    //endregion

    //region getters
    public long getId() {
        return id;
    }

    public String getArticle() {
        return article;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public Group getGroup() {
        return group;
    }

    public Type getType() {
        return type;
    }

    public BrandModel getBrand() {
        return brand;
    }
    //endregion

    //region setters
    public void setId(long id) {
        this.id = id;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setBrand(BrandModel brand) {
        this.brand = brand;
    }
    //endregion

    //region overrides
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductModel that = (ProductModel) o;
        return Double.compare(that.getPrice(), getPrice()) == 0 &&
                Objects.equals(getArticle(), that.getArticle()) &&
                Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPrice(), getArticle(), getDescription());
    }
    //endregion
}
