package ru.filit.train.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Модель данных заказа
 * */
public class OrderModel {

    //region fields
    private long id;
    private long customerId;
    private long sellerId;
    private double generalPrice;
    private double restPrice;
    private LocalDate creationDate;
    private int deliveryStatus;
    private boolean paid;
    private ArrayList<ProductModel> products;
    //endregion

    //region getters
    public long getId() {
        return id;
    }

    public long getCustomerId() {
        return customerId;
    }

    public long getSellerId() {
        return sellerId;
    }

    public double getGeneralPrice() {
        return generalPrice;
    }

    public double getRestPrice() {
        return restPrice;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public int getDeliveryStatus() {
        return deliveryStatus;
    }

    public boolean isPaid() {
        return paid;
    }

    public ArrayList<ProductModel> getProducts(){
        return products;
    }
    //endregion

    //region setters
    public void setId(long id) {
        this.id = id;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public void setGeneralPrice(double generalPrice) {
        this.generalPrice = generalPrice;
    }

    public void setRestPrice(double restPrice) {
        this.restPrice = restPrice;
    }

    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public void setProducts(ArrayList<ProductModel> products) {
        this.products = products;
    }
    //endregion

    //region overrides
    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        OrderModel that = (OrderModel) o;
        return getCustomerId() == that.getCustomerId() &&
                getSellerId() == that.getSellerId() &&
                Double.compare(that.getGeneralPrice(), getGeneralPrice()) == 0 &&
                Double.compare(that.getRestPrice(), getRestPrice()) == 0 &&
                getDeliveryStatus() == that.getDeliveryStatus() &&
                isPaid() == that.isPaid() &&
                Objects.equals(getProducts(), that.getProducts());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCustomerId(), getSellerId(), getGeneralPrice(), getRestPrice(),
                getDeliveryStatus(), isPaid(), getProducts());
    }
    //endregion
}
