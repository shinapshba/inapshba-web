package ru.filit.train.utils;

import ru.filit.train.exceptios.CustomException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Функционал считывания файлов свойств
 * */
public class PropertiesReader {
    private static final Logger logger = Logger.getLogger(PropertiesReader.class.getName());

    /**
     * Реализация чтения файла свойств
     * @param propertyFileName имя файла свойств
     * @return карта полученных свойств
     * */
    public static Properties read(String propertyFileName){
        try{
            logger.info(String.format("Reading %s ...", propertyFileName));
            InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertyFileName);
            Properties properties = new Properties();
            properties.load(in);
            assert in != null;
            in.close();
            logger.info(String.format("%s was read successfully", propertyFileName));
            return properties;
        } catch (IOException | NullPointerException io_e){
            throw new CustomException(String.format("Error in reading %s", propertyFileName), io_e);
        }
    }
}
