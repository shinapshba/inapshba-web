package ru.filit.train.utils;

import ru.filit.train.exceptios.CustomException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Генератор хэш значений паролей пользователей
 * */
public class PasswordHashGenerator {
    /**
     * Основной функционал генератора
     * @param passwordToHash пароль, который нужно хэшировать
     * @param salt соль для усложнения хэширования
     * */
    public static String generate(String passwordToHash, String salt){
        String generatedPassword;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NullPointerException | NoSuchAlgorithmException e){
            throw new CustomException("Cannot generate password hash", e);
        }
        return generatedPassword;
    }
}
