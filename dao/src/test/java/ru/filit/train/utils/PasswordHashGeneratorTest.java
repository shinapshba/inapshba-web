package ru.filit.train.utils;

import ru.filit.train.exceptios.CustomException;
import org.junit.Test;

import static org.junit.Assert.*;

public class PasswordHashGeneratorTest {

    @Test(expected = CustomException.class)
    public void generate() {
        String hash = PasswordHashGenerator.generate("pass", "SH-12");
        assertTrue(hash.length() > 10);
        hash = PasswordHashGenerator.generate(null, null);
        assertNull(hash);
    }
}