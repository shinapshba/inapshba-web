package ru.filit.train.utils;

import ru.filit.train.exceptios.CustomException;
import org.junit.Test;

import static org.junit.Assert.*;

public class PropertiesReaderTest {

    private static final String PROPERTY_FILE_NAME = "test_property.properties";
    private static final String PROPERTY_NAME = "developer";
    private static final String DEVELOPER = "shaur@inapshba";

    @Test(expected = CustomException.class)
    public void read(){
        assertEquals(DEVELOPER, PropertiesReader.read(PROPERTY_FILE_NAME).getProperty(PROPERTY_NAME));
        PropertiesReader.read("no_name");
    }
}