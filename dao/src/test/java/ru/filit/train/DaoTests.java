package ru.filit.train;

import ru.filit.train.model.AccountModelTest;
import ru.filit.train.model.OrderModelTest;
import ru.filit.train.model.ProductModelTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.filit.train.model.dim.BrandModelTest;
import ru.filit.train.repository.*;
import ru.filit.train.utils.PasswordHashGeneratorTest;
import ru.filit.train.utils.PropertiesReaderTest;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        BrandModelTest.class,
        AccountModelTest.class,
        OrderModelTest.class, ProductModelTest.class,
        PasswordHashGeneratorTest.class, PropertiesReaderTest.class,
        BrandRepositoryTest.class,
        AccountRepositoryTest.class,
        OrderRepositoryTest.class,
        ProductRepositoryTest.class,
        ConnectionSupplierTest.class
})
public class DaoTests {

}