package ru.filit.train.repository;

import org.junit.Assert;
import ru.filit.train.model.AccountModel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class AccountRepositoryTest {

    private ArrayList<AccountModel> models = new ArrayList<>();
    private AccountRepository repository;
    private AccountRepository repository_WrongScheme;

    @Before
    public void setUp() {
        repository_WrongScheme = new AccountRepository("test_WRONG_scheme.properties");
        repository = new AccountRepository("test_scheme.properties");
        models = new ArrayList<>();
        models.add(createAccount(1));
        models.add(createAccount(2));
        models.add(createAccount(3));
    }

    @Test
    public void login(){
        AccountModel model = createAccount(0);
        repository.save(model);
        repository.login("Login-0", "TestHash0");

        try{
            repository.login("No Login--1", "TestHash0");
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository.login("Login-0", "WrongHash0");
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.login("", "");
            fail("Expected exception");
        } catch (Exception ignored) { }

        repository.removeById(repository.findLast().getId());
    }

    @Test
    public void complex(){
        models.forEach(model -> repository.save(model));
        Assert.assertEquals(models.get(0).getLogin(), repository.findByLogin(models.get(0).getLogin()).getLogin());
        try{
            repository.save(models.get(0));
            fail("Expected exception");
        } catch (Exception ignored) { }

        ArrayList<AccountModel> models_db = (ArrayList<AccountModel>) repository.findAll();
        boolean isSuccess = models_db.containsAll(models);
        assertTrue(isSuccess);

        for (AccountModel model : models_db) {
            if (models.contains(model)) {
                models.get(models.indexOf(model)).setId(model.getId());
            }
        }
        models.forEach(model -> repository.removeById(model.getId()));

        repository.save(models.get(0));
        AccountModel model_db = repository.findLast();
        model_db.setLogin("New Login");
        repository.update(model_db);
        model_db = repository.findById(model_db.getId());
        assertEquals("New Login", model_db.getLogin());
        repository.removeById(model_db.getId());

        try{
            repository_WrongScheme.removeById(1);
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findAll();
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findById(1);
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findLast();
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.update(model_db);
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findByLogin("Login");
            fail("Expected exception");
        } catch (Exception ignored) { }
    }

    private AccountModel createAccount(int postPrefix){
        AccountModel model = new AccountModel();
        model.setLogin(String.format("Login-%d", postPrefix));
        model.setEmail(String.format("email_%d@test.ru", postPrefix));
        model.setPhone(String.format("899999999%d", postPrefix));
        model.setPhoneConfirmed(false);
        model.setEmailConfirmed(false);
        model.setActivity(true);
        model.setRole(1);
        model.setPassword_hash(String.format("TestHash%d", postPrefix));
        return model;
    }
}