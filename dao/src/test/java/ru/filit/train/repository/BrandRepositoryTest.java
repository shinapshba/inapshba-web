package ru.filit.train.repository;

import ru.filit.train.model.dim.BrandModel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class BrandRepositoryTest {

    private static BrandRepository repository;
    private static BrandRepository repository_WrongScheme;
    private static ArrayList<BrandModel> models;

    @Before
    public void setUp() {
        repository = new BrandRepository("test_scheme.properties");
        repository_WrongScheme = new BrandRepository("test_WRONG_scheme.properties");
        models = new ArrayList<>();
        models.add(new BrandModel("Test-1"));
        models.add(new BrandModel("Test-2"));
        models.add(new BrandModel("Test-3"));
    }

    @Test
    public void complex(){

        models.forEach(model -> repository.save(model));

        try{
            repository.save(models.get(0));
            fail("Expected exception");
        } catch (Exception ignored) { }

        ArrayList<BrandModel> models_db = (ArrayList<BrandModel>) repository.findAll();
        boolean isSuccess = models_db.containsAll(models);
        assertTrue(isSuccess);

        for (BrandModel model : models_db) {
            if (models.contains(model)) {
                models.get(models.indexOf(model)).setId(model.getId());
            }
        }
        models.forEach(model -> repository.removeById(model.getId()));

        repository.save(models.get(0));
        String oldName = models.get(0).getName();
        BrandModel model_db = repository.findByName(oldName);
        assertEquals(model_db, repository.findLast());
        model_db.setName("New Name");
        repository.update(model_db);
        model_db = repository.findById(model_db.getId());
        assertEquals("New Name", model_db.getName());
        repository.removeById(model_db.getId());

        try{
            repository_WrongScheme.removeById(1);
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findByName("No Name");
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findAll();
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findById(1);
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findLast();
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.update(model_db);
            fail("Expected exception");
        } catch (Exception ignored) { }
    }
}