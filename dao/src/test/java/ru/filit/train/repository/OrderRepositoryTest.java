package ru.filit.train.repository;

import ru.filit.train.model.OrderModel;
import ru.filit.train.model.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class OrderRepositoryTest {

    private OrderRepository repository;
    private OrderRepository repository_WrongScheme;
    private ArrayList<OrderModel> models;

    @Before
    public void setUp() {
        repository = new OrderRepository("test_scheme.properties");
        repository_WrongScheme = new OrderRepository("test_WRONG_scheme.properties");
        models = new ArrayList<>();
        models.add(createOrder(1));
        models.add(createOrder(2));
        models.add(createOrder(3));
    }

    @Test
    public void complex(){
        models.forEach(model -> repository.save(model));
        ArrayList<OrderModel> models_db = (ArrayList<OrderModel>) repository.findAll();
        assertTrue(models_db.size() >= models.size());

        for(int i = 0; i < models.size(); i++){
            repository.removeById(repository.findLast().getId());
        }

        repository.save(models.get(0));
        OrderModel model_db = repository.findLast();
        model_db.setDeliveryStatus(3);
        repository.update(model_db);
        model_db = repository.findById(model_db.getId());
        assertEquals(3, model_db.getDeliveryStatus());
        try {
            model_db.getProducts().add(Mockito.mock(ProductModel.class));
            repository.update(model_db);
        } catch (Exception ignored) { }
        repository.removeById(model_db.getId());

        try{
            OrderModel model = createOrder(4);
            model.setProducts(new ArrayList<>());
            model.getProducts().add(Mockito.mock(ProductModel.class));
            repository.save(model);
            fail("Expected exception");
        } catch (Exception ignored) { }
        repository.removeById(repository.findLast().getId());

        try{
            repository_WrongScheme.removeById(1);
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findAll();
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findById(1);
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findLast();
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.update(model_db);
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{

            repository_WrongScheme.update(model_db);
            fail("Expected exception");
        } catch (Exception ignored) { }
    }

    private OrderModel createOrder(int uniqNum){
        OrderModel model = new OrderModel();
        model.setSellerId(1);
        model.setCustomerId(2);
        model.setGeneralPrice(5000 * uniqNum);
        model.setRestPrice(5000 * uniqNum);
        model.setPaid(false);
        model.setDeliveryStatus(1);
        model.setProducts(new ArrayList<>());
        return model;
    }
}