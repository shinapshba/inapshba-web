package ru.filit.train.repository;

import ru.filit.train.model.ProductModel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ProductRepositoryTest {

    private ProductRepository repository;
    private ProductRepository repository_WrongScheme;
    private ArrayList<ProductModel> models;

    @Before
    public void setUp() {
        repository = new ProductRepository("test_scheme.properties");
        repository_WrongScheme = new ProductRepository("test_WRONG_scheme.properties");
        models = new ArrayList<>();
        models.add(new ProductModel());
        models.add(new ProductModel());

        models.get(0).setId(1);
        models.get(0).setDescription("Description");
        models.get(0).setArticle("FGR-234");
        models.get(0).setPrice(1200);

        models.get(1).setId(2);
        models.get(1).setDescription("Test description");
        models.get(1).setArticle("SH-1999");
        models.get(1).setPrice(15000);
    }

    @Test
    public void complex(){
        ArrayList<ProductModel> models_db = (ArrayList<ProductModel>) repository.findAll();
        if(models_db.size() != models.size()){
            fail("The sizes of the lists don't match");
        }
        assertArrayEquals(models_db.toArray(), models.toArray());

        try{
            repository_WrongScheme.findAll();
            fail("Expected exception");
        } catch (Exception ignored) { }

        ProductModel model = repository.findById(1);
        String oldArticle = model.getArticle();
        model.setArticle("TEST");
        repository.update(model);
        ProductModel model_db = repository.findById(1);
        assertEquals("TEST", model_db.getArticle());

        try{
            repository_WrongScheme.update(model);
            fail("Expected exception");
        } catch (Exception ignored) { }

        model_db.setArticle(oldArticle);
        repository.update(model_db);

        model = repository.findById(1);
        try{
            repository.save(model);
            fail("Expected exception");
        } catch (Exception ignored) { }

        try{
            repository_WrongScheme.findById(model.getId());
            fail("Expected exception");
        } catch (Exception ignored) { }

        model.setArticle("ART-12");
        repository.save(model);
        model_db = repository.findLast();
        try{
            repository_WrongScheme.findLast();
            fail("Expected exception");
        } catch (Exception ignored) { }
        model.setId(model_db.getId());
        assertEquals(model, model_db);

        try{
            repository_WrongScheme.removeById(model_db.getId());
            fail("Expected exception");
        } catch (Exception ignored) { }
        repository.removeById(model_db.getId());
    }
}