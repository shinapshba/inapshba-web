package ru.filit.train.repository;

import ru.filit.train.exceptios.CustomException;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.junit.Assert.*;

public class ConnectionSupplierTest {

    private static final String WRONG_SCHEME = "test_WRONG_scheme.properties";

    @Test(expected = CustomException.class)
    public void getConnection() throws Exception {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        Connection connection = connectionSupplier.getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT 1 FROM dual";
        ResultSet resultSet = statement.executeQuery(query);
        if(resultSet.next()){
            assertEquals(1, resultSet.getInt(1));
        } else {
            fail("Expected not empty result set");
        }
        ConnectionSupplier connectionSupplier_wrong = new ConnectionSupplier(WRONG_SCHEME);
        connectionSupplier_wrong.getConnection();
    }
}