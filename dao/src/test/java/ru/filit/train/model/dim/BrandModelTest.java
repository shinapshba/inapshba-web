package ru.filit.train.model.dim;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BrandModelTest {

    private static BrandModel model;
    private static final long ID = 321;
    private static final String NAME = "test_name";

    @Before
    public void setUp(){
        model = new BrandModel();
        model.setName(NAME);
        model.setId(ID);
    }

    @Test
    public void constructors(){
        BrandModel model_raw = new BrandModel();
        BrandModel modelWithName = new BrandModel("Name");
        BrandModel model = new BrandModel(123, "Gora");
        assertEquals(0, model_raw.getId());
        assertNull(model_raw.getName());
        assertEquals(123, model.getId());
        assertEquals("Gora", model.getName());
        assertEquals(modelWithName.getName(), "Name");
    }

    @Test
    public void getters(){
        assertEquals(ID, model.getId());
        assertEquals(NAME, model.getName());
    }

    @Test
    public void overrides(){
        BrandModel model_1 = new BrandModel(1, "Test");
        BrandModel model_2 = new BrandModel(1, "Test");
        assertEquals(model_1, model_2);
        assertNotEquals(0, model_1.hashCode());
    }
}