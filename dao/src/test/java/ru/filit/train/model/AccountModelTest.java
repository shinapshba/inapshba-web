package ru.filit.train.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.time.LocalDate;


public class AccountModelTest {

    private static final long ID = 123;
    private static final String PASSWORD_HASH = "sdef@hfgeuejhnc9789mjnk_JHBVGVjo";
    private static final String EMAIL = "test@test.ru";
    private static final String LOGIN = "login_test";
    private static final String PHONE = "89657777777";
    private static final int ROLE = 3;
    private static final boolean EMAIL_CONFIRMED = false;
    private static final boolean PHONE_CONFIRMED = false;
    private static final boolean ACTIVITY = true;
    private static final LocalDate REGISTRATION_DATE = LocalDate.now();
    private static AccountModel model;

    @Before
    public void setUp() {
        model = new AccountModel();
        model.setLogin(LOGIN);
        model.setEmail(EMAIL);
        model.setPhone(PHONE);
        model.setPhoneConfirmed(PHONE_CONFIRMED);
        model.setEmailConfirmed(EMAIL_CONFIRMED);
        model.setActivity(ACTIVITY);
        model.setRole(ROLE);
        model.setId(ID);
        model.setRegistrationDate(REGISTRATION_DATE);
        model.setPassword_hash(PASSWORD_HASH);
    }

    @Test
    public void getters(){
        assertEquals(ID, model.getId());
        assertEquals(PASSWORD_HASH, model.getPassword_hash());
        assertEquals(LOGIN, model.getLogin());
        assertEquals(EMAIL, model.getEmail());
        assertEquals(PHONE, model.getPhone());
        assertEquals(ROLE, model.getRole());
        assertEquals(REGISTRATION_DATE, model.getRegistrationDate());
        assertEquals(ACTIVITY, model.isActive());
        assertEquals(EMAIL_CONFIRMED, model.isEmailConfirmed());
        assertEquals(PHONE_CONFIRMED, model.isPhoneConfirmed());
    }

    @Test
    public void overrides(){
        AccountModel model_1 = new AccountModel();
        model_1.setId(123);
        AccountModel model_2 = new AccountModel();
        model_2.setId(123);
        assertEquals(model_1, model_2);
        assertNotEquals(0, model.hashCode());
    }
}