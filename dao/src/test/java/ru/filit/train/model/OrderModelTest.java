package ru.filit.train.model;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class OrderModelTest {

    private static OrderModel model;
    private static final long ID = 123;
    private static final LocalDate CREATION_DATE = LocalDate.now();
    private static final long CUSTOMER_ID = 555;
    private static final long SELLER_ID = 777;
    private static final int DELIVERY_STATUS = 2;
    private static final double GENERAL_PRICE = 25000.25;
    private static final double REST_PRICE = 10000;
    private static final boolean PAID = false;

    @SuppressWarnings("unchecked")
    private static final ArrayList<ProductModel> PRODUCTS = mock(ArrayList.class);

    private static final double DOUBLE_DELTA = 0.01;

    @Before
    public void setUp() {
        model = new OrderModel();
        model.setId(ID);
        model.setCreationDate(CREATION_DATE);
        model.setCustomerId(CUSTOMER_ID);
        model.setSellerId(SELLER_ID);
        model.setGeneralPrice(GENERAL_PRICE);
        model.setRestPrice(REST_PRICE);
        model.setDeliveryStatus(DELIVERY_STATUS);
        model.setPaid(PAID);
        model.setProducts(PRODUCTS);
    }

    @Test
    public void getters(){
        assertEquals(ID, model.getId());
        assertEquals(CREATION_DATE, model.getCreationDate());
        assertEquals(CUSTOMER_ID, model.getCustomerId());
        assertEquals(SELLER_ID, model.getSellerId());
        assertEquals(GENERAL_PRICE, model.getGeneralPrice(), DOUBLE_DELTA);
        assertEquals(REST_PRICE, model.getRestPrice(), DOUBLE_DELTA);
        assertEquals(DELIVERY_STATUS, model.getDeliveryStatus());
        assertEquals(PAID, model.isPaid());
        assertEquals(PRODUCTS, model.getProducts());
    }

    @Test
    public void overrides(){
        OrderModel model_1 = new OrderModel();
        OrderModel model_2 = new OrderModel();
        model_1.setId(1);
        model_2.setId(1);
        assertEquals(model_1, model_2);
        assertNotEquals(0, model.hashCode());
    }
}