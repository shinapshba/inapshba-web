package ru.filit.train.model;

import ru.filit.train.model.dim.BrandModel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class ProductModelTest {

    private static ProductModel model;
    private static final long ID = 745;
    private static final String ARTICLE = "TRE-123";
    private static final double PRICE = 1999.99;
    private static final String DESCRIPTION = "Detail description";
    private static final ProductModel.Type TYPE = new ProductModel.Type(1, "test_type");
    private static final ProductModel.Group GROUP = new ProductModel.Group(2, "test_group");
    private static final BrandModel BRAND = mock(BrandModel.class);

    private static final double DOUBLE_DELTA = 0.01;

    @Before
    public void setUp() {
        model = new ProductModel();
        model.setId(ID);
        model.setArticle(ARTICLE);
        model.setPrice(PRICE);
        model.setDescription(DESCRIPTION);
        model.setGroup(GROUP);
        model.setType(TYPE);
        model.setBrand(BRAND);
    }

    @Test
    public void getters(){
        assertEquals(ID, model.getId());
        assertEquals(ARTICLE, model.getArticle());
        assertEquals(PRICE, model.getPrice(), DOUBLE_DELTA);
        assertEquals(DESCRIPTION, model.getDescription());
        assertEquals(GROUP, model.getGroup());
        assertEquals(TYPE, model.getType());
        assertEquals(BRAND, model.getBrand());
    }

    @Test
    public void overrides(){
        ProductModel model_1 = new ProductModel();
        ProductModel model_2 = new ProductModel();
        model_1.setId(1);
        model_2.setId(1);
        assertEquals(model_1, model_2);
        assertNotEquals(0, model.hashCode());
    }
}